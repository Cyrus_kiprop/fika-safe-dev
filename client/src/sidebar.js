import { css } from "emotion";
const styles = {
  but: css`
    padding: 32px;
    background-color: hotpink;
    font-size: 24px;
    border-radius: 4px;
    &:hover {
      color: white;
    }
  `,
  middle: css`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `,
  menu: css`
    width: 300px;
    border-radius: 8px;
    overflow: hidden;
  `,
  item: css`
    border-top: 1px solid #2980b9;
    overflow: hidden;
  `,
  btn: css`
    display: block;
    padding: 16px 20px;
    background: $3498db;
    color: white;
    &:before {
      content: "";
      position: relative;
      width: 14px;
      height: 14px;
      background: #2498db;
      left: 20px;
      bottom: -7px;
      transform: rotate(45deg;);
    }
  `
};
export default styles;
