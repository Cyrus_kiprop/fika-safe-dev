import React from "react";
import { middle, menu, item } from "./sidebar.js";

const Sidebar = params => {
  return (
    <div className={middle}>
      <div className={menu}>
        <li className={item}>
          <a href="#" className="btn">
            <i className="far fa-user"></i>
            Messages
          </a>
          <div className="smenu">
            <a href="#">Posts</a>
            <a href="#">Picture</a>
          </div>
        </li>

        <li className={item}>
          <a href="#" className="btn">
            <i className="far fa-user"></i>
            profile
          </a>
          <div className="smenu">
            <a href="#">new</a>
            <a href="#">Sent</a>
            <a href="#">Spam</a>
          </div>
        </li>

        <li className={item}>
          <a href="#" className="btn">
            <i className="far fa-user"></i>
            Setting
          </a>
          <div className="smenu">
            <a href="#">Password</a>
            <a href="#">Language</a>
          </div>
        </li>

        <li className="item">
          <a href="#" className="btn">
            Logout
          </a>
        </li>
      </div>
    </div>
  );
};

export default Sidebar;
