// create another router for getting 'product' resources
let Router = require("express").Router();
const { Rider } = require("../../models/models.js");
const ObjectId = require("mongodb").ObjectID;

//filter image files
// const fileFilter = (req, file, cb) => {
//   // reject a file
//   if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//     cb(null, true);
//   } else {
//     cb(null, false);
//   }
// };
//api/riders
Router.post(`/`,(req, res) => {
  const newRider = req.body;

  Rider.create(newRider)
    .then(result => {
      Rider.findById({ _id: result.insertedId }).then(addedRider => {
        res.json(addedRider);
      });
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ message: `Internal Server Error: ${error}` });
    });
});

/* GET AND DELETE RIDER WITH SPECIFIC ID */
Router.delete(`/id/:id`, (req, res) => {
    let ridersId;
    try {
      ridersId = new ObjectId(req.params.id);
    } catch (error) {
      res.status(400).send({ message: `Invalid riders ID:${ridersId}` });
    }
    // THE REQ.BODY IS OPTIONAL INTHE FINDBYIDANREMOVE METHOD
    Rider.findByIdAndRemove({ _id: ridersId }, req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        console.log({
          message: `Unable to delelete the riders profile ${err}`
        });
      });
  })

  Router.put(`/id/:id`, (req, res) => {
    let ridersId;
    try {
      ridersId = new ObjectId(req.params.id);
    } catch (error) {
      res.status(400).send({ message: `Invalid riders ID:${ridersId}` });
    }
    const newRider = req.body;
    Rider.findByIdAndUpdate({ _id: ridersId }, newRider)
      .find({ _id: ridersId })
      .then(updatedRider => {
        res.json(updatedRider);
      })
      .catch(err => {
        console.log(err);
        res
          .status(500)
          .json({ message: `Unable to update the riders information ${err}` });
      });
  })

  Router.get(`/id/:id`, (req, res) => {
    let ridersId;
    try {
      console.log(req.params.id);
      ridersId = new ObjectId(req.params.id);
      console.log(` this is the id ${ridersId}`);
    } catch (error) {
      res.status(400).send({ message: `Invalid riders ID:${ridersId}` });
    }
    Rider.findById({ _id: ridersId })
      .then(rider => {
        if (!rider)
          res.status(404).json({ message: `No such Rider: ${ridersId}` });
        else res.json(rider);
        console.log(rider.riderPassportPhoto);
      })
      .catch(error => {
        console.log(error);
        res.status(500).json({ message: `Internal Server Error: ${error}` });
      });
  });

// Router.post(`/`, uploadingle("riderPassportPhoto")).post((req, res, next) => {
//   const riders = new Rider({
//     riderPassportPhoto: req.file.path,
//     ...req.body
//   });
//   riders
//     .save()
//     .then(result => {
//       console.log(result);
//       res.status(201).json({
//         message: "Registered Rider successfully",
//         createdProduct: {
//           riderFname: result.riderFname,
//           _id: result._id,
//           request: {
//             type: "GET",
//             url: "http://localhost:3000/riders/" + result._id
//           }
//         }
//       });
//     })
//     .catch(err => {
//       console.log(err);
//       res.status(500).json({
//         error: err
//       });
//     });
// });

/* GET ALL RIDERS */

Router.get(`/email/:email`,(req, res) => {
  let email;
  try {
    email = req.params.email;
  } catch (error) {
    res.status(400).send({ message: `Invalid email:${email}` });
  }
  Rider.find()
    .populate({
      path: "sacco",
      match: { email: email },
      select: "name -_id"
    })
    .then(rider => {
      if (!rider)
        res.status(404).json({ message: "No avilable Riders in the system" });
      else {
        console.log(rider);
        let saccoData = [];
        // ensures that the data is sacco Specific
        rider.map(item => (item.sacco !== null ? saccoData.push(item) : false));
        res.json(saccoData);
      }
    })
    .catch(error => {
      console.log(error);
      res.status(500).json({ message: `Internal Server Error: ${error}` });
    });
});

module.exports = Router;
