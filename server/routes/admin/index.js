let Router = require("express").Router();
const { UserModel } = require("../../models/models.js");

// Admin registration endpoint --> /api/admin/register
Router.post("/register", async (request, response) => {
  try {
    const user = new UserModel(request.body);
    const result = await user.save();
    response.send(result);
  } catch (error) {
    response.status(500).send(error.message);
  }
});

// aminstrator login
Router.post(`/login`,(req, res) => {
  const { email, password } = req.body;
  console.log("User submitted: ", email, password);
  const bcrypt = require("bcrypt");
  const jwt = require("jsonwebtoken");

  UserModel.findOne({ email: email }).then(user => {
    console.log("User Found: ", user);
    if (user === null) {
      res.json(false);
    }
    bcrypt.compare(password, user.password, function(err, result) {
      if (result === true) {
        console.log("Valid!");
        let token = jwt.sign({ email: user.email }, "keyboard cat 4 ever", {
          expiresIn: 129600
        }); // Signing the token
        res.json({
          sucess: true,
          err: null,
          token
        });
      } else {
        console.log("Entered Password and Hash do not match!");
        res.status(401).json({
          sucess: false,
          token: null,
          err: "Entered Password and Hash do not match!"
        });
      }
    });
  });
});

module.exports = Router;
